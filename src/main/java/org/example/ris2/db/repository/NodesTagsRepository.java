package org.example.ris2.db.repository;

import lombok.RequiredArgsConstructor;
import org.example.ris2.db.helpers.DBHelper;

import java.sql.SQLException;

@RequiredArgsConstructor
public class NodesTagsRepository {
    private final DBHelper dbHelper;

    public void insert(long nodeId, long tagId, String tagValue) throws SQLException {
        dbHelper.getStringHelper().insert("nodes_tags", nodeId, tagId, tagValue);
    }

    public void insertPrepared(long nodeId, long tagId, String tagValue) throws SQLException {
        dbHelper.getPreparedHelper().insert("nodes_tags", nodeId, tagId, tagValue);
    }
}
