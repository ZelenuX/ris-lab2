package org.example.ris2.db.repository;

import lombok.RequiredArgsConstructor;
import org.example.ris2.db.helpers.DBHelper;
import org.example.ris2.db.entity.TagEntity;
import org.openstreetmap.osm._0.Tag;

import java.sql.SQLException;

@RequiredArgsConstructor
public class TagsRepository {
    private final DBHelper dbHelper;
    private Long nextId;

    public TagEntity save(Tag tag) throws SQLException {
        var existingId = dbHelper.getStringHelper().executeWrappingMapperException(
                "select id from tags where key = '" + tag.getK() + "';",
                rs -> rs.next() ? rs.getLong("id") : null
        );
        if (existingId != null) { //already in db
            return new TagEntity(existingId, tag.getK());
        }

        var id = newId();
        var sb = new StringBuilder("insert into tags (id, key) values (");
        sb.append(id).append(", '").append(tag.getK()).append("');");
        dbHelper.getStringHelper().execute(sb.toString());
        return new TagEntity(id, tag.getK());
    }

    public TagEntity savePrepared(Tag tag) throws SQLException {
        var existingId = dbHelper.getPreparedHelper().executeWrappingMapperException(
                "select id from tags where key = ?;",
                rs -> rs.next() ? rs.getLong("id") : null,
                tag.getK()
        );
        if (existingId != null) { //already in db
            return new TagEntity(existingId, tag.getK());
        }

        var id = newId();
        dbHelper.getPreparedHelper().execute("insert into tags (id, key) values (?, ?) on conflict do nothing;", id, tag.getK());
        return new TagEntity(id, tag.getK());
    }

    private long newId() throws SQLException {
        if (nextId == null) {
            nextId = dbHelper.getStringHelper().executeWrappingMapperException(
                    "select max(id) as max_id from tags;",
                    rs -> rs.next() ? rs.getLong("max_id") : 1
            );
        }
        return nextId++;
    }
}
