package org.example.ris2.db.repository;

import lombok.RequiredArgsConstructor;
import org.example.ris2.db.helpers.DBHelper;

import java.sql.SQLException;

@RequiredArgsConstructor
public class UsersRepository {
    private final DBHelper dbHelper;

    public void save(Long id, String name) throws SQLException {
        dbHelper.getStringHelper().insertIfDoesNotExist("users", id, name);
    }

    public void savePrepared(Long id, String name) throws SQLException {
        dbHelper.getPreparedHelper().insertIfDoesNotExist("users", id, name);
    }
}
