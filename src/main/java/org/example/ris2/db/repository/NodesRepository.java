package org.example.ris2.db.repository;

import lombok.RequiredArgsConstructor;
import org.example.ris2.db.helpers.DBHelper;
import org.openstreetmap.osm._0.Node;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Objects;

@RequiredArgsConstructor
public class NodesRepository {
    private final DBHelper dbHelper;

    public void insert(Node node) throws SQLException {
        dbHelper.getStringHelper().insert(
                "nodes",
                node.getId(),
                node.getLat(),
                node.getLon(),
                node.getUid(),
                Objects.equals(true, node.isVisible()),
                node.getVersion(),
                node.getChangeset(),
                node.getTimestamp().toString()
        );
    }

    public void insertPrepared(Node node) throws SQLException {
        dbHelper.getPreparedHelper().insert(
                "nodes",
                node.getId(),
                node.getLat(),
                node.getLon(),
                node.getUid(),
                Objects.equals(true, node.isVisible()),
                node.getVersion(),
                node.getChangeset(),
                Timestamp.from(Instant.parse(node.getTimestamp().toString()))
        );
    }
}
