package org.example.ris2.db.util;

import java.sql.SQLException;

public interface RunnableThrowingSQLException {
    void run() throws SQLException;
}
