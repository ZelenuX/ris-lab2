package org.example.ris2.db.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Triple<T1, T2, T3> {
    private T1 v1;
    private T2 v2;
    private T3 v3;
}
