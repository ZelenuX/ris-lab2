package org.example.ris2.db.util;

import java.sql.SQLException;

public interface FunctionThrowingSQLException<T, R> {
    R apply(T arg) throws SQLException;
}
