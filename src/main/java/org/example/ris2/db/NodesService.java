package org.example.ris2.db;

import lombok.RequiredArgsConstructor;
import org.example.ris2.db.helpers.DBHelper;
import org.example.ris2.db.repository.NodesRepository;
import org.example.ris2.db.repository.NodesTagsRepository;
import org.example.ris2.db.repository.TagsRepository;
import org.example.ris2.db.repository.UsersRepository;
import org.example.ris2.db.util.Triple;
import org.openstreetmap.osm._0.Node;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class NodesService {
    private static final int MAX_BATCHES = 10;

    private final DBHelper dbHelper;
    private final NodesRepository nodesRepository;
    private final UsersRepository usersRepository;
    private final TagsRepository tagsRepository;
    private final NodesTagsRepository nodesTagsRepository;
    private final List<Node> nodesForTagsBatchedInsertion = new ArrayList<>();
    private int batchesCounter = 0;

    public void save(Node node) throws SQLException {
        dbHelper.executeTransaction(() -> {
            usersRepository.save(node.getUid().longValue(), node.getUser());
            nodesRepository.insert(node);
            node.getTag().forEach(t -> {
                try {
                    var tagEntity = tagsRepository.save(t);
                    nodesTagsRepository.insert(node.getId().longValue(), tagEntity.getId(), t.getV());
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            });
        });
    }

    public void savePrepared(Node node) throws SQLException {
        dbHelper.executeTransaction(() -> {
            usersRepository.savePrepared(node.getUid().longValue(), node.getUser());
            nodesRepository.insertPrepared(node);
            node.getTag().forEach(t -> {
                try {
                    var tagEntity = tagsRepository.savePrepared(t);
                    nodesTagsRepository.insertPrepared(node.getId().longValue(), tagEntity.getId(), t.getV());
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            });
        });
    }

    public void savePreparedBatched(Node node) throws SQLException {
        dbHelper.executeTransaction(() -> {
            usersRepository.savePrepared(node.getUid().longValue(), node.getUser());
            nodesRepository.insertPrepared(node);
            nodesForTagsBatchedInsertion.add(node);
            if (++batchesCounter >= MAX_BATCHES) {
                var nodeTagsToInsert = new ArrayList<Triple<Long, Long, String>>();//List<node.id, tag.id, tag_value>
                nodesForTagsBatchedInsertion.forEach(n ->
                        n.getTag().forEach(t -> {
                            try {
                                var tagEntity = tagsRepository.savePrepared(t);
                                nodeTagsToInsert.add(new Triple<>(n.getId().longValue(), tagEntity.getId(), t.getV()));
                            } catch (SQLException e) {
                                throw new RuntimeException(e);
                            }
                        }));
                nodeTagsToInsert.forEach(nt -> {
                    try {
                        nodesTagsRepository.insertPrepared(nt.getV1(), nt.getV2(), nt.getV3());
                    } catch (SQLException e) {
                        throw new RuntimeException(e);
                    }
                });
                dbHelper.flushBatched();
                nodesForTagsBatchedInsertion.clear();
                batchesCounter = 0;
            }
        });
    }

    public void flushBatched() throws SQLException {
        dbHelper.flushBatched();
    }
}
