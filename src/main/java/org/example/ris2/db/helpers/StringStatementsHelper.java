package org.example.ris2.db.helpers;

import lombok.RequiredArgsConstructor;
import org.example.ris2.db.util.FunctionThrowingSQLException;

import java.sql.*;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;

@RequiredArgsConstructor
public class StringStatementsHelper {
    private final Connection connection;
    private final DatabaseMetaData dbMetaData;
    private final Map<String, String> insertStatements = new HashMap<>();//tableName -> sql
    private Statement stmnt;

    public <T> T executeWrappingMapperException(String statement, FunctionThrowingSQLException<ResultSet, T> mapper) throws SQLException {
        var res = getStatement().executeQuery(statement);
        try {
            return mapper.apply(res);
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void execute(String statement) throws SQLException {
        getStatement().execute(statement);
    }

    public void insert(String tableName, Object... values) throws SQLException {
        this.execute(generateInsertString(false, tableName, values));
    }

    public void insertIfDoesNotExist(String tableName, Object... values) throws SQLException {
        this.execute(generateInsertString(true, tableName, values));
    }

    public void close() throws SQLException {
        if (stmnt != null) {
            stmnt.close();
        }
    }


    private String generateInsertString(boolean doNothingIfExists, String tableName, Object... values) throws SQLException {
        var exception = new AbstractMap.SimpleEntry<Integer, SQLException>(0, null);
        var sql = insertStatements.computeIfAbsent(tableName, tn -> {
            try {
                var columns = dbMetaData.getColumns(null, null, tableName, null);
                columns.next();
                var sb = new StringBuilder("insert into ").append(tableName)
                        .append(" (").append(columns.getString("COLUMN_NAME"));
                while (columns.next()) {
                    sb.append(", ").append(columns.getString("COLUMN_NAME"));
                }
                sb.append(") values (");
                return sb.toString();
            }
            catch (SQLException e) {
                exception.setValue(e);
                return null;
            }
        });
        if (exception.getValue() != null) {
            throw new SQLException(exception.getValue());
        }

        var sb = new StringBuilder(sql);
        if (values[0] instanceof String) {
            sb.append("'").append(values[0]).append("'");
        }
        else {
            sb.append(values[0]);
        }
        for (int i = 1; i < values.length; ++i) {
            sb.append(", ");
            if (values[i] instanceof String) {
                sb.append("'").append(values[i]).append("'");
            }
            else {
                sb.append(values[i]);
            }
        }
        return sb.append(")").append(doNothingIfExists ? " on conflict do nothing;" : ";").toString();
    }

    private Statement getStatement() throws SQLException {
        if (stmnt == null) {
            stmnt = connection.createStatement();
        }
        return stmnt;
    }

}
