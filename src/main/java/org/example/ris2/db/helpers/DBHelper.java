package org.example.ris2.db.helpers;

import org.example.ris2.db.util.RunnableThrowingSQLException;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;

public class DBHelper implements AutoCloseable {
    private final Connection connection;
    private final DatabaseMetaData dbMetaData;
    private final StringStatementsHelper stringHelper;
    private final PreparedStatementsHelper preparedHelper;

    public DBHelper(Connection connection, boolean batchedPreparedHelper) throws SQLException {
        this.connection = connection;
        this.dbMetaData = connection.getMetaData();
        this.stringHelper = new StringStatementsHelper(connection, dbMetaData);
        this.preparedHelper = new PreparedStatementsHelper(connection, dbMetaData, batchedPreparedHelper);
        connection.setAutoCommit(false);
    }

    public StringStatementsHelper getStringHelper() {
        return stringHelper;
    }

    public PreparedStatementsHelper getPreparedHelper() {
        return preparedHelper;
    }

    public void executeFromResource(String resourcePath) throws SQLException, IOException {
        try (var sqlFile = getClass().getResourceAsStream(resourcePath)) {
            var sql = new String(sqlFile.readAllBytes());
            executeTransaction(() -> stringHelper.execute(sql));
        }
    }

    public void executeTransaction(RunnableThrowingSQLException action) throws SQLException {
        try {
            action.run();
            connection.commit();
        }
        catch (SQLException e) {
            connection.rollback();
            throw new SQLException(e);
        }
    }

    public void flushBatched() throws SQLException {
        preparedHelper.flushBatched();
    }


    @Override
    public void close() throws SQLException {
        stringHelper.close();
        preparedHelper.close();
        connection.close();
    }
}
