package org.example.ris2.db.helpers;

import lombok.RequiredArgsConstructor;
import org.example.ris2.db.util.FunctionThrowingSQLException;

import java.math.BigInteger;
import java.sql.*;
import java.util.*;

@RequiredArgsConstructor
public class PreparedStatementsHelper {
    private static final Map<Class<?>, Integer> sqlTypeConstants = new HashMap<>();
    static {
        sqlTypeConstants.put(Integer.class, Types.INTEGER);
        sqlTypeConstants.put(Long.class, Types.BIGINT);
        sqlTypeConstants.put(BigInteger.class, Types.BIGINT);
        sqlTypeConstants.put(Boolean.class, Types.BOOLEAN);
        sqlTypeConstants.put(Double.class, Types.DOUBLE);
        sqlTypeConstants.put(Timestamp.class, Types.TIMESTAMP);
    }

    private final Map<String, String> insertStatements = new HashMap<>();//tableName -> sql
    private final Map<String, PreparedStatement> preparedStatements = new HashMap<>();//sql -> preparedStatement
    private final List<PreparedStatement> batchedStatements = new ArrayList<>();
    private final Connection connection;
    private final DatabaseMetaData dbMetaData;
    private final boolean batchedInsertion;

    public <T> T executeWrappingMapperException(String sql, FunctionThrowingSQLException<ResultSet, T> mapper, Object... params) throws SQLException {
        var stmnt = getStatement(sql);
        for (int i = 0; i < params.length; i++) {
            setStmntObject(stmnt, i + 1, params[i]);
        }
        var res = stmnt.executeQuery();
        try {
            return mapper.apply(res);
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void execute(String sql, Object... params) throws SQLException {
        var stmnt = getStatement(sql);
        for (int i = 0; i < params.length; i++) {
            setStmntObject(stmnt, i + 1, params[i]);
        }
        if (batchedInsertion) {
            stmnt.addBatch();
            if (!batchedStatements.contains(stmnt)) {
                batchedStatements.add(stmnt);
            }
        }
        else {
            stmnt.execute();
        }
    }

    public void insert(String tableName, Object... values) throws SQLException {
        this.execute(generateInsertString(false, tableName), values);
    }

    public void insertIfDoesNotExist(String tableName, Object... values) throws SQLException {
        this.execute(generateInsertString(true, tableName), values);
    }

    public void close() throws SQLException {
        for (var stmnt : preparedStatements.values()) {
            stmnt.close();
        }
    }

    public void flushBatched() throws SQLException {
        for (var stmnt : batchedStatements.toArray(new PreparedStatement[0])) {
            stmnt.executeBatch();
        }
    }


    private String generateInsertString(boolean doNothingIfExists, String tableName) throws SQLException {
        var exception = new AbstractMap.SimpleEntry<Integer, SQLException>(0, null);
        var sql = insertStatements.computeIfAbsent(tableName, tn -> {
            try {
                int colNumber = 1;
                var columns = dbMetaData.getColumns(null, null, tableName, null);
                columns.next();
                var sb = new StringBuilder("insert into ").append(tableName)
                        .append(" (").append(columns.getString("COLUMN_NAME"));
                while (columns.next()) {
                    sb.append(", ").append(columns.getString("COLUMN_NAME"));
                    ++colNumber;
                }

                sb.append(") values (?");
                for (int i = 1; i < colNumber; ++i) {
                    sb.append(", ?");
                }
                return sb.append(")").toString();
            }
            catch (SQLException e) {
                exception.setValue(e);
                return null;
            }
        });
        if (exception.getValue() != null) {
            throw new SQLException(exception.getValue());
        }
        return sql + (doNothingIfExists ? " on conflict do nothing;" : ";");
    }

    private PreparedStatement getStatement(String sql) throws SQLException {
        var exception = new AbstractMap.SimpleEntry<Integer, SQLException>(1, null);
        var res = preparedStatements.computeIfAbsent(sql, s -> {
            try {
                return connection.prepareStatement(sql);
            }
            catch (SQLException e) {
                exception.setValue(e);
                return null;
            }
        });
        if (exception.getValue() != null) {
            throw new SQLException(exception.getValue());
        }
        return res;
    }

    private void setStmntObject(PreparedStatement stmnt, int index, Object obj) throws SQLException {
        var sqlType = sqlTypeConstants.get(obj.getClass());
        if (sqlType != null) {
            stmnt.setObject(index, obj, sqlType);
        }
        else {
            stmnt.setObject(index, obj);
        }
    }

}
