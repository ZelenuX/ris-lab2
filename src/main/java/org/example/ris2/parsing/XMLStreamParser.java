package org.example.ris2.parsing;

import org.example.ris2.Main;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.util.StreamReaderDelegate;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class XMLStreamParser {
    private XMLEventReaderUnmarshaller unmarshaller;
    private Map<Class<?>, List<Consumer<Object>>> listeners = new HashMap<>();
    private List<Consumer<Object>> defaultListeners = new ArrayList<>();
    private boolean unmarshallingAllStopped;

    public XMLStreamParser(InputStream in, String xmlClassesPackageName) throws XMLStreamException, JAXBException {
        var factory = XMLInputFactory.newInstance();
        var reader = factory.createXMLEventReader(new XsiTypeReader(factory.createXMLStreamReader(in)));

        var context = JAXBContext.newInstance(xmlClassesPackageName);
        this.unmarshaller = new XMLEventReaderUnmarshaller(reader, context.createUnmarshaller());
    }

    public void goInsideFirstOfName(String localName) throws XMLStreamException {
        unmarshaller.goInsideFirstOfName(localName);
    }

    public <T> void addListener(Class<T> c, Consumer<T> action) {
        listeners.computeIfAbsent(c, key -> new ArrayList<>()).add(((Consumer<Object>) action));
    }

    public void addDefaultListener(Consumer<Object> action) {
        defaultListeners.add(action);
    }

    public Object unmarshall() throws JAXBException, XMLStreamException {
        var element = unmarshaller.unmarshall();
        if (element == null) {
            return null;
        }
        listeners.getOrDefault(element.getClass(), defaultListeners)
                .forEach(action -> action.accept(element));
        return element;
    }

    public void unmarshalAll() throws JAXBException, XMLStreamException {
        unmarshallingAllStopped = false;
        while (!unmarshallingAllStopped) {
            var element = unmarshaller.unmarshall();
            if (element == null) {
                break;
            }
            listeners.getOrDefault(element.getClass(), defaultListeners)
                    .forEach(action -> action.accept(element));
        }
    }

    public int unmarshalAllAndIgnoreErrored() throws XMLStreamException {
        unmarshallingAllStopped = false;
        int erroredElements = 0;
        while (!unmarshallingAllStopped) {
            try {
                var element = unmarshaller.unmarshall();
                if (element == null) {
                    break;
                }
                listeners.getOrDefault(element.getClass(), defaultListeners)
                        .forEach(action -> action.accept(element));
            }
            catch (JAXBException ignored) {
                ++erroredElements;
            }
        }
        return erroredElements;
    }

    public void stopUnmarshallingAll() {
        unmarshallingAllStopped = true;
    }

    private static class XsiTypeReader extends StreamReaderDelegate {
        public XsiTypeReader(XMLStreamReader reader) {
            super(reader);
        }

//        @Override
//        public String getAttributeNamespace(int arg0) {
//            return "http://openstreetmap.org/osm/0.6";
//        }

//        @Override
//        public String getNamespaceURI() {
//            return "http://openstreetmap.org/osm/0.6";
//        }

//        @Override
//        public String getNamespaceURI(int index) {
//            return index == 0 ? "http://openstreetmap.org/osm/0.6" : null;
//        }
//
//        @Override
//        public String getNamespacePrefix(int index) {
//            return null;
//        }
    }
}
