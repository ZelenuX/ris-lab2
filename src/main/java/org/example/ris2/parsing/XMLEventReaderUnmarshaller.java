package org.example.ris2.parsing;

import javax.xml.bind.JAXBException;
import javax.xml.bind.UnmarshalException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import java.io.StringReader;

public class XMLEventReaderUnmarshaller {
    private final XMLEventReader reader;
    private final Unmarshaller unmarshaller;

    public XMLEventReaderUnmarshaller(XMLEventReader reader, Unmarshaller unmarshaller) {
        this.reader = reader;
        this.unmarshaller = unmarshaller;
    }

    public void goInsideFirstOfName(String localName) throws XMLStreamException {
        while (reader.hasNext()) {
            var event = reader.nextEvent();
            if (event.isStartElement() && localName.equals(event.asStartElement().getName().getLocalPart())) {
                break;
            }
        }
    }

    public Object unmarshall() throws XMLStreamException, JAXBException {
        StringBuilder sb = new StringBuilder();
        String rootName = null;
        while (reader.hasNext()) {
            var element = reader.nextEvent();
            if (element.isStartElement()) {
                var startElement = element.asStartElement();
                if (rootName == null) {
                    rootName = startElement.getName().getLocalPart();
                }
                else {
                    sb.append('\n');
                }
                sb.append(startElement.toString())
                        .insert(sb.length() - 1, " xmlns=\"http://openstreetmap.org/osm/0.6\"");
            }
            if (element.isEndElement()) {
                var endElement = element.asEndElement();
                if (rootName == null) {
                    return null;
                }
                sb.append(endElement.toString());
                if (endElement.getName().getLocalPart().equals(rootName)) {
                    break;
                }
            }
        }
        var elementString = sb.toString();
        try {
            return unmarshaller.unmarshal(new StringReader(elementString));
        }
        catch (UnmarshalException e) {
            throw new UnmarshalException(e);
        }
    }
}
