package org.example.ris2;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.example.ris2.db.helpers.DBHelper;
import org.example.ris2.db.NodesService;
import org.example.ris2.db.repository.NodesRepository;
import org.example.ris2.db.repository.NodesTagsRepository;
import org.example.ris2.db.repository.TagsRepository;
import org.example.ris2.db.repository.UsersRepository;
import org.example.ris2.parsing.XMLStreamParser;
import org.openstreetmap.osm._0.Bounds;
import org.openstreetmap.osm._0.Node;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {
    private static final String path = "C:\\Users\\il_ya\\Downloads\\RU-NVS.osm.bz2";
    private static final Integer numberOfNodes = 10000;
    private static int mode; //1,2,3 - with insertion, else - just unmarshall

    private static final String DB_URL = "jdbc:postgresql://localhost:5432/ris2";
    private static final String DB_USER = "ris2_user";
    private static final String DB_PASSWORD = "ris2_password";
    private static final String DB_Driver = "org.postgresql.Driver";

    public static void main(String[] args) {
        testStrategy(0);
        testStrategy(1);
        testStrategy(2);
        testStrategy(3);
    }

    private static void testStrategy(int strategyNumber) {
        mode = strategyNumber;
        var nodesNumber = new AtomicInteger(0);
        try {
            Class.forName(DB_Driver);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("JDBC driver(" + DB_Driver + ") not found");
            return;
        }
        try (
                var in = new BZip2CompressorInputStream(new BufferedInputStream(new FileInputStream(path)));
                var dbHelper = new DBHelper(DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD), mode == 3)
        ) {
            var nodesService = prepareDBAndDAOs(dbHelper);

            var parser = prepareParser(in, nodesService, nodesNumber);

            parser.goInsideFirstOfName("osm");
            var startTime = System.currentTimeMillis();
            int errored = parser.unmarshalAllAndIgnoreErrored();
            var endTime = System.currentTimeMillis();

//            System.out.println("Nodes: " + nodesNumber.get());
//            System.out.println("Errored elements: " + errored);
            var totalTimeMs = (float) (endTime - startTime);
            System.out.println(mode + ") " + totalTimeMs / nodesNumber.get() + " ms/node"
                    + " (total: " + totalTimeMs / 1000 + " s)");
        }
        catch (Exception e) {
            System.out.println("Nodes until exception: " + nodesNumber.get());

            e.printStackTrace();
        }
    }

    private static NodesService prepareDBAndDAOs(DBHelper dbHelper) throws SQLException, IOException {
        dbHelper.executeFromResource("/db_clear.sql");
        dbHelper.executeFromResource("/db_init.sql");
        return new NodesService(
                dbHelper,
                new NodesRepository(dbHelper),
                new UsersRepository(dbHelper),
                new TagsRepository(dbHelper),
                new NodesTagsRepository(dbHelper)
        );
    }

    private static XMLStreamParser prepareParser(InputStream in,
                                                 NodesService nodesService,
                                                 AtomicInteger nodesNumber) throws JAXBException, XMLStreamException {
        var parser = new XMLStreamParser(in, "org.openstreetmap.osm._0");
        parser.addListener(Bounds.class, bounds -> {});
        parser.addListener(Node.class, node -> {
            try {
                switch (mode) {
                    case 1:
                        nodesService.save(node);
                        break;
                    case 2:
                        nodesService.savePrepared(node);
                        break;
                    case 3:
                        nodesService.savePreparedBatched(node);
                        break;
                    default:
                        if (node.getChangeset().intValue() == -1) throw new SQLException();
                }

                var newNodesNumber = nodesNumber.incrementAndGet();
                if (numberOfNodes != null && numberOfNodes >= 0 && newNodesNumber >= numberOfNodes) {
                    parser.stopUnmarshallingAll();
                }
            }
            catch (SQLException e) {
                System.out.println("ERROR: SQLException occurred.");
                throw new RuntimeException(e);
            }
        });
        parser.addDefaultListener(element -> {
            try {
                parser.stopUnmarshallingAll();
                nodesService.flushBatched();
            } catch (SQLException e) {
                System.out.println("ERROR: SQLException occurred.");
                throw new RuntimeException(e);
            }
        });
        return parser;
    }
}
