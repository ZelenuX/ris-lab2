create table users (
    id bigint primary key,
    name varchar(128)
);

create table tags (
    id bigint primary key,
    key varchar(64)
);

create table members (
    id bigint primary key,
    type varchar(16),
    ref bigint,
    role varchar(128)
);

create table nodes (
    id bigint primary key,
    lat float,
    long float,
    user_id bigint,
    visible boolean,
    version bigint,
    changeset bigint,
    timestamp timestamptz,
    foreign key (user_id) references users(id)
);

create table nodes_tags (
    node_id bigint,
    tag_id bigint,
    tag_value varchar(256),
    primary key (node_id, tag_id),
    foreign key (node_id) references nodes(id),
    foreign key (tag_id) references tags(id)
);

create table relations (
    id bigint primary key,
    user_id bigint,
    visible boolean,
    version bigint,
    changeset bigint,
    timestamp timestamp,
    foreign key (user_id) references users(id)
);

create table relations_members (
    relation_id bigint,
    member_id bigint,
    primary key (relation_id, member_id),
    foreign key (relation_id) references relations(id),
    foreign key (member_id) references members(id)
);

create table relations_tags (
    relation_id bigint,
    tag_id bigint,
    primary key (relation_id, tag_id),
    foreign key (relation_id) references relations(id),
    foreign key (tag_id) references tags(id)
);

create table ways (
    id bigint primary key,
    user_id bigint,
    visible boolean,
    version bigint,
    changeset bigint,
    timestamp timestamp,
    foreign key (user_id) references users(id)
);

create table ways_nds (
    way_id bigint,
    nd_ref bigint,
    primary key (way_id, nd_ref),
    foreign key (way_id) references ways(id)
);

create table ways_tags (
    way_id bigint,
    tag_id bigint,
    primary key (way_id, tag_id),
    foreign key (way_id) references ways(id),
    foreign key (tag_id) references tags(id)
);

-- create table osms (
--     id integer primary key,
--     version float,
--     generator varchar(128),
--     minlat float,
--     minlon float,
--     maxlat float,
--     maxlon float
-- );
--
-- create table osms_nodes (
--     osm_id bigint,
--     node_id bigint,
--     primary key (osm_id, node_id),
--     foreign key (osm_id) references osms(id),
--     foreign key (node_id) references nodes(id)
-- );
--
-- create table osms_ways (
--     osm_id bigint,
--     way_id bigint,
--     primary key (osm_id, way_id),
--     foreign key (osm_id) references osms(id),
--     foreign key (way_id) references ways(id)
-- );
--
-- create table osms_relations (
--     osm_id bigint,
--     relation_id bigint,
--     primary key (osm_id, relation_id),
--     foreign key (osm_id) references osms(id),
--     foreign key (relation_id) references relations(id)
-- );
